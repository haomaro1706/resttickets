<?php
use Src\Rest;

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
header("Access-Control-Allow-Methods: POST");
header("Allow: POST");

include __DIR__.'/vendor/autoload.php';

$rest = new Rest();