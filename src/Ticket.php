<?php
namespace Src;

use Src\Conexion;

class Ticket{

    public function crear($json){
        $conexion = new Conexion();

        $sql = "insert into public.ticket (usuario,estado) values ('{$json['usuario']}','A')";
        return $conexion->insertar($sql);        
    }

    public function editar($json){
        $conexion = new Conexion();

        $sql = "update public.ticket set estado='{$json['estado']}' where id={$json['id']};";
        return $conexion->actualizar($sql);        
    }

    public function todos($json){
        $conexion = new Conexion();

        $sql = "select * from public.ticket;";
        return $conexion->todos($sql);        
    }

    public function borrar($json){
        $conexion = new Conexion();

        $sql = "delete from public.ticket where id={$json['id']};";
        return $conexion->borrar($sql);
    }    

}