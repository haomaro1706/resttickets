<?php
namespace Src;

class Conexion{

    public $host;
    public $db;
    public $user;
    public $pass;
    public $conexion;
    public $url;

    function __construct(){
        $this->conectar();
    }


    public function cargarValores(){

        $this->host='localhost';
        $this->db='tickets';
        $this->user='postgres';
        $this->pass='';
        $this->conexion = 'host='.$this->host.' dbname='.$this->db.' user='.$this->user.' password='.$this->pass;
    }


    public function conectar(){

        $this->cargarValores();

        $this->url=pg_connect($this->conexion);

        //return true;

    }

    public function destruir(){

        pg_close($this->url);

    }

    public function insertar($sql){
        $mensaje = '';
        $result =  pg_query($this->url, $sql);
        if (!$result) {
            $mensaje = pg_last_error($this->url);
        }else {
            $mensaje = "Datos insertados.";
        }
        return $mensaje;
    }

    public function actualizar($sql){
        $mensaje = '';
        $result =  pg_query($this->url, $sql);
        if (!$result) {
            $mensaje = pg_last_error($this->url);
        }else {
            $mensaje = "Datos actualizados.";
        }
        return $mensaje;
    }

    public function todos($sql){
        $mensaje = '';
        $result =  pg_query($this->url, $sql);
        if (!$result) {
            $mensaje = pg_last_error($this->url);
        }else {
            $mensaje = [];
            while ($row = pg_fetch_row($result)) {
                $mensaje[] = [
                    "id"=>$row[0],
                    "usuario"=> $row[1],
                    "fecha_creacion"=> $row[2],
                    "fecha_actualizacion"=> $row[3],
                    "estado"=> $row[4]
                ];
            }
        }
        return $mensaje;
    }

    public function borrar($sql){
        $mensaje = '';
        $result =  pg_query($this->url, $sql);
        if (!$result) {
            $mensaje = pg_last_error($this->url);
        }else {
            $mensaje = 'Datos eliminados.';
        }
        return $mensaje;
    }

}