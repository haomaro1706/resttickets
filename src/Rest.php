<?php

namespace Src;

class Rest{

    private $json;


    public function __construct()
    {
        $this->validar();
        $this->acciones();
    }

    public function validar(){
        if($this->validarPost()){
            $handler = fopen('php://input', 'r');
            $request = stream_get_contents($handler);
            
            $this->json = json_decode($request, true);
        }
    }

    private function throwError($code, $message){
        $dato = array(
            'code' => $code,
            'resp' => $message,
            'status' => 'error'
        );

        http_response_code($code);
        header('content-type: application/json');
        $errorMsg = json_encode($dato);
        echo $errorMsg;
        exit;
    }

    public function returnResponse($response)
    {
        http_response_code(200);
        header('content-type: application/json');
        
        $response = ['respuesta'=>$response];

        $response = json_encode($response);
        echo $response;
        exit;
    }

    private function validarPost(){
        $mensaje = null;
        //print_r($_SERVER);
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($_SERVER['CONTENT_TYPE'] !== 'application/json') {
                //echo 'jum';
                $mensaje = 'Contenido enviado invalido';
            }
        }else{
            //echo 'por ak';
            $mensaje = ('Metodo de envio invalido');
        }

        if(!isset($_SERVER['PHP_AUTH_USER']) ){
            //$mensaje = ('No se enviaron credenciales');
        }

        if(!is_null($mensaje)){
            $this->throwError(401,$mensaje);
        }

        return true;
    }

    private function acciones(){
        $ticket = new Ticket();
        if($this->json['metodo']=='crear'){
            $respuesta = $ticket->crear($this->json);
            $this->returnResponse($respuesta);
        }else if($this->json['metodo']=='editar'){
            $respuesta = $ticket->editar($this->json);
            $this->returnResponse($respuesta);
        }else if($this->json['metodo']=='todos'){
            $respuesta = $ticket->todos($this->json);
            $this->returnResponse($respuesta);
        }else if($this->json['metodo']=='borrar'){
            $respuesta = $ticket->borrar($this->json);
            $this->returnResponse($respuesta);
        }
    }

}